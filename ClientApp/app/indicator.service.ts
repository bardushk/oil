﻿import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()

/** This class is intended for components interacting */
export class IndicatorService {

    /** EDIT MODE state */
    private editMode = new BehaviorSubject<boolean>(false);

    /** Process indicator */
    private waiting = new BehaviorSubject<boolean>(false);

    /** Observable to subscribe in component */
    editMode$ = this.editMode.asObservable();

    /** Observable for waiting indicator */
    waiting$ = this.waiting.asObservable();

    /**
     * Event handler to fire on EDIT MODE change 
     * @param mode EDIT MODE value
     */
    toggleEditMode(mode: boolean) {
        this.editMode.next(mode);
    }

    /**
     * Event handler to toggle waiting mode
     * @param waiting boolean
     */
    toggleWaiting(waiting: boolean) {
        this.waiting.next(waiting);
    }

}
