﻿import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Indicator } from './indicator';

@Injectable()
 /** The service contains method to exchange data with web-api */
export class ApiService {
    /** Web-api url */
    private url: string;

    /**
     * Constructor
     * @param http
     * @param baseUrl
     */
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string) {
        this.url = baseUrl + 'api/indicators';
    }

    /**
     * Performs request to get list of indicators
     */
    getList() {
        return this.http.get(this.url);
    }

    /**
     * Performs request to get detailed data by indicator id 
     * @param id
     */
    getItem(id: string) {
        return this.http.get(`${this.url}/${id}`);
    }

    /**
     * Performs request to save indicator data
     * @param indicator
     */
    save(indicator: Indicator) {
        if (indicator.id) {
            return this.http.put(`${this.url}/${indicator.id}`, indicator);
        }
        return this.http.post(this.url, indicator);
    }

    /**
     * Perform request to delete indicator
     * @param id
     */
    delete(id: string) {
        return this.http.delete(`${this.url}/${id}`);
    }
}
