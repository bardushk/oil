﻿
import { Injectable } from '@angular/core';

@Injectable()
/** Validating rules */
export class ValidatorService {

    /**
     * Indicator Name validatior. Name is required
     * @param value
     */
    checkName(value: string): boolean {
        if (!value) {
            return false;
        }
        return true;
    }

    /**
     * MinValue validatior. 
     * @param value
     */
    checkMinValue(value: number): boolean {
        return true;
    } 

    /**
     * MaxValue validator.
     * @param value
     */
    checkMaxValue(value: number): boolean {
        return true;
    }

    /**
     * Description validator.
     * @param value
     */
    checkDescription(value: string): boolean {
        return true;
    }

    /**
     * Value validator.
     * @param value
     */
    checkValue(value: number): boolean {
        return true;
    }
}
