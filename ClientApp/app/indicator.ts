﻿/**
 * Indicator data structure
 */
export class Indicator {
    id: string;
    name: string;
    value: number;
    minValue: number;
    maxValue: number;
    level: number;
    valueName: string;
    description: string;
    alert: boolean;

    constructor() {
        this.value = 0;
        this.level = 0;
        this.minValue = 0;
        this.maxValue = 100;
    }
}