import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { HomeComponent } from './components/home/home.component';
import { DetailComponent } from './components/detail/detail.component';
import { AboutComponent } from './components/about/about.component';

import { IndicatorComponent } from './components/indicator/indicator.component';
import { ApiService } from './api.service';
import { IndicatorService } from './indicator.service';
import { ValidatorService } from './validator.service';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        DetailComponent,
        AboutComponent,
        IndicatorComponent
    ],
    providers: [
        ApiService,
        IndicatorService,
        ValidatorService
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'about', component: AboutComponent },
            { path: 'detail/:id', component: DetailComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModuleShared {
}
