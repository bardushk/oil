import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Indicator } from '../../indicator';
import { ApiService } from '../../api.service';
import { IndicatorService } from '../../indicator.service';
import { fadeInAnimation } from '../../_animations/fade.animation';

@Component({
    selector: 'home',
    styleUrls: ['home.component.css'],
    templateUrl: './home.component.html',

    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' }
})
/** Home page component */
export class HomeComponent implements OnDestroy {

    /** To unsubscripe from infinite Observable  */
    private subscription: Subscription;

    /** To hold indicators data received from api*/
    indicatorList: Indicator[];

    /** To track EDIT MODE from parent app.component */
    editMode: boolean = false;

    /**
     * Constructor
     * @param apiService - to get data from api
     * @param indicatorService - to watch for EDIT MODE in parent component
     */
    constructor(
        private apiService: ApiService,
        private indicatorService: IndicatorService
    ) {
    }

    /**
     * To get data from api
     */
    getData() {
        this.indicatorService.toggleWaiting(true);
        this.apiService.getList().subscribe(result => {
            this.indicatorList = result.json() as Indicator[];
            this.indicatorService.toggleWaiting(false);
        }, error => {
            console.error(error);
            this.indicatorService.toggleWaiting(false);
        });
    }

    /**
     *  To set indicator list data on component init
     */
    ngOnInit() {
        this.getData();
        this.subscription = this.indicatorService.editMode$.subscribe(mode => this.editMode = mode);
    }

    /**
     * To delete indicator
     * @param id indicator unique identfier 
     */
    onDeleteClick(id: string) {
        this.indicatorService.toggleWaiting(true);
        this.apiService.delete(id).subscribe(r => {
            this.getData();
            this.indicatorService.toggleWaiting(false);
        });
    }

    /**
     * To prevent from memory leak when component destroyed
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
