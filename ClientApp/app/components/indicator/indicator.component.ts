﻿import { Component, Input } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { Indicator } from '../../indicator';

@Component({
    selector: 'oil-indicator',
    templateUrl: './indicator.component.html',
    styleUrls: ['./indicator.component.css']
})
 /** Indicator reusable component  */
export class IndicatorComponent {
    @Input() indicator: Indicator;
}
