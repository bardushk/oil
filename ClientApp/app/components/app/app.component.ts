import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { IndicatorService } from '../../indicator.service';
import { fadeInAnimation } from '../../_animations/fade.animation';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' }
})

/** Main app component */
export class AppComponent {

    /** EDIT MODE status, initially non-edit */
    editMode: boolean = false;

    /** Waiting indicator visibility */
    waiting: boolean = false;

    /** To unsubscribe on component destroy */
    private subscription: Subscription;

    /**
     * Constructor
     * @param indicatorService - to send EDIT MODE value to child-components
     */
    constructor(private indicatorService: IndicatorService) {
    }

    /**
     * To get subscription on waiting Observable
     */
    ngOnInit() {
        this.subscription = this.indicatorService.waiting$.subscribe(waiting => this.waiting = waiting);
    }

    /**
     *  To unsubscribe
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    /**
     * To toggle EDIT MODE status on EDIT MODE button click
     */
    onEditModeClick() {
        this.editMode = !this.editMode;
        this.indicatorService.toggleEditMode(this.editMode);
    }
}
