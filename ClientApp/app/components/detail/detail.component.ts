﻿import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import { Indicator } from '../../indicator';
import { ApiService } from '../../api.service';
import { IndicatorService } from '../../indicator.service';
import { ValidatorService } from '../../validator.service';
import { fadeInAnimation } from '../../_animations/fade.animation';

@Component({
    selector: 'detail',
    templateUrl: 'detail.component.html',
    styleUrls: ['detail.component.css'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' }
})

/** Details page component */
export class DetailComponent implements OnDestroy {
    /** Unique id of indicator or 'new' for new one */
    private id: any;

    /** To unsubscribe from indicator service on component destroy */
    private subscription: Subscription; 

    /** Indicator to be displayed */
    indicator: Indicator;

    /** EDIT MODE status */
    editMode: boolean = false;

    /**
     * Constructor
     * @param validatorService - to validate user input
     * @param apiService - to get data from api
     * @param indicatorService - to get EDIT MODE from parent component
     * @param route - to get indicator id from URI
     * @param location - to navigate to privious page
     */
    constructor(
        public validatorService: ValidatorService,
        private apiService: ApiService,
        private indicatorService: IndicatorService,
        private route: ActivatedRoute,
        private location: Location
    ) {
        this.indicator = new Indicator();
    }

    /**
     * Get data from api on component init
     */
    ngOnInit() {
        this.subscription = this.indicatorService.editMode$.subscribe(mode => this.editMode = mode);
        this.id = this.route.snapshot.paramMap.get('id');
        if (this.id === 'new') {
            return;
        }
        this.apiService.getItem(this.id).subscribe(result => { this.indicator = result.json() as Indicator });
    }

    /**
     * Update level on indicator parameters change
     */
    onChange() {
        let indicator = this.indicator;
        let level = (indicator.value - indicator.minValue) / (indicator.maxValue - indicator.minValue) * 100;
        this.indicator.alert = false;
        if (level < 0) {
            this.indicator.alert = true;
            level = 0;
        }
        if (level > 100) {
            this.indicator.alert = true;
            level = 100;
        }
        this.indicator.level = level;
    }

    /**
     * To prevent memory leak when component destroyed
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    /**
     * To save indicator data
     */
    onSubmit() {
        if (!this.validatorService.checkName(this.indicator.name)) {
            return;
        }
        if (!this.validatorService.checkValue(this.indicator.value)) {
            return;
        }
        if (!this.validatorService.checkMinValue(this.indicator.value)) {
            return;
        }
        this.apiService.save(this.indicator).subscribe(result => this.location.back());
    }
}
