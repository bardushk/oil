﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace oiltanks.Models
{
    public class oiltanksContext : DbContext
    {
        public oiltanksContext (DbContextOptions<oiltanksContext> options)
            : base(options)
        {
        }

        public DbSet<oiltanks.Models.Tank> Tank { get; set; }
    }
}
