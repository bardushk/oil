﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using oiltanks.Models;

namespace oiltanks.Controllers
{
    
    [Produces("application/json")]
    [Route("api/Indicators")]
    public class IndicatorsController : Controller
    {
        private readonly int DEFAULT_LIMIT = 10;
        private readonly int RAND_LIMIT = 100;

        private readonly oiltanksContext _context;

        public IndicatorsController(oiltanksContext context)
        {
            _context = context;
        }

        // GET: api/Indicators
        [HttpGet]
        public IEnumerable<Indicator> GetIndicator()
        {
            var tanks = _context.Tank.Take<Tank>(DEFAULT_LIMIT);
            var random = new Random();

            var indicators = tanks.Select(t => 
               new Indicator()
                {
                    ID = t.ID,
                    Name = t.Name,
                    MinValue = t.MinValue,
                    MaxValue = t.MaxValue,
                    Description = t.Description,
                    Value = random.Next(0, RAND_LIMIT),
                }
            ).ToArray<Indicator>();
            for (int i = 0; i < indicators.Count(); i++) {
                var indicator = indicators[i];
                double level = (double) (indicator.Value - indicator.MinValue) / (indicator.MaxValue - indicator.MinValue) * 100;
                if (level > 100)
                {
                    level = 100;
                    indicators[i].Alert = true;
                }
                if (level < 0)
                {
                    level = 0;
                    indicators[i].Alert = true;
                }
                indicators[i].Level = level;
            }
            return indicators;
        }

        // GET: api/Indicators/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetIndicator([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var indicator = await _context.Tank.SingleOrDefaultAsync(m => m.ID == id);

            if (indicator == null)
            {
                return NotFound();
            }

            return Ok(indicator);
        }

        // PUT: api/Indicators/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIndicator([FromRoute] Guid id, [FromBody] Tank indicator)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != indicator.ID)
            {
                return BadRequest();
            }

            _context.Entry(indicator).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IndicatorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(indicator);
        }

        // POST: api/Indicators
        [HttpPost]
        public async Task<IActionResult> PostIndicator([FromBody] Tank indicator)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tank.Add(indicator);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetIndicator", new { id = indicator.ID }, indicator);
        }

        // DELETE: api/Indicators/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteIndicator([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var indicator = await _context.Tank.SingleOrDefaultAsync(m => m.ID == id);
            if (indicator == null)
            {
                return NotFound();
            }

            _context.Tank.Remove(indicator);
            await _context.SaveChangesAsync();

            return Ok(indicator);
        }

        private bool IndicatorExists(Guid id)
        {
            return _context.Tank.Any(e => e.ID == id);
        }
    }
}