﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace oiltanks.Models
{
    public class Tank
    {
        public System.Guid ID { get; set; }
        public string Name { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public string Description { get; set; }
    }
}
