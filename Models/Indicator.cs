﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace oiltanks.Models
{
    public class Indicator
    {
        public System.Guid ID { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public double Level { get; set; }
        public string Description { get; set; }
        public bool Alert { get; set; }
        public string AlertDescription { get; set; }
    }
}
